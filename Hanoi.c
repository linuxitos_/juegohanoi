/**
 * Created by linuxitos
 * */
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <curses.h>
#include <time.h>
#include <unistd.h>

#define D 5

#define MAX 8
#define ENT 10
#define RET 7
int **torres, topeA, topeB, topeC;
int m;

/*Declaración de variables globales*/
char **torre;
int **planostorres, numdiscos, todis,filas, columnas;
int esquinaA, esquinaB,esquinaC;
/*Variables de los mensajes*/
char origen, destino;
int disco;


typedef struct{
int arreglo[MAX];
int tope;
}Pila;

void PilaVacia (Pila *a);
int EsVacia (Pila *a);
int Cima (Pila a);
void Push (int x, Pila *a);
int Pop (Pila *a); 
void imtorres(int numdis);
void llenarpila(Pila *p1);


void Centrar(char *cad, int r);

void Derecha(char *cad, int r);

/*Declaración de funciones y procedimientos*/
void Hanoi(char a,char b, char c,int n);
void build_torres(void);/*En esta función se crea una matriz de Nfilas x 3columnas la cual simula el juego de las torres de hanoi.*/
void mover_disco(int x_origen, int x_destino, int* topeOrigen, int* topeDestino);
/*Este bloque de módulos genara una torre indice de donde se puede seleccionar un nivel y imprimirlo.*/
void definir_planos(void);
void draw_planos(void);
void draw_torres(void);

void jugarha(int numdiscos,char nom[15],time_t *now,time_t *des);
/*Esta funciones imprimen las torres*/
void show_hanoi(int origen, int destino);
void pintar_torre(void);
void imprimir_nivel(int nivel, int fila, int columna);
void iniciar_esquinas(void);
void paleta_colores(void);
void leerdiscos(int *numdiscos);
void leertorre(int *tor,int i,char *me);

void imprimirprese(void );
void draw_menubar(WINDOW *menubar);


void Centrar(char *cad, int r);

void Derecha(char *cad, int r);
void LeeNombre(char *cad,int max,int r, int c);
void Error(char *cad, int r);
void LeeEntero(char *cad,int max,int r, int c);
int LeeInt(int r, int c);
int Menu1();
int Rango(int men , int may,int r,int c);
int AvisoConfirmacion(char *cad);
void titulo(char *impre,int r1, int r2, int r3);
void iniciando();

#define ML 5
#define P 5

typedef struct {
	int posicion;
	char *nombre;
	int tiempo;
}Rancking;

void InsertarDato(Rancking *lista, char *cad, int time, int disco);
void LeeImpreRancking(Rancking *lista,int ndisco, int opc);
void RecorrerDato(Rancking *lista, int ind);
void LeerRancking (Rancking *lista, char *disco);
void LlenandoEstruc(Rancking *lista, char *cad, int *total);
void ImprimirRancking (Rancking *lista, int ndisco, int opc, char *disco, char *nivel);
void GuardarRancking(Rancking *lista, char *disco);
void MandarImpress(Rancking *lista, int nd, int opm);


int main()
{
	char nom[15];
	Rancking lista[ML];
	time_t now,des;
    double DIF;
	int opm,nd,av2,dis;
	initscr();
	start_color();
	paleta_colores();
	do{
		bkgd(COLOR_PAIR(8));
		clear();
		Centrar("TORRES DE HANOI",1);
		opm=Menu1();
		switch(opm){
			case 1:
				bkgd(COLOR_PAIR(8));
				clear();
				titulo("Hanoi",0,2,4);
				av2=AvisoConfirmacion("¿Leer instrucciones? s para leer,cualquier tecla para salir");
				if(av2==1){
					imprimirprese();
				}
				clear();
				titulo("Hanoi",0,2,4);
				mvprintw(7,10,"Ingresa tu nombre de jugador");
				LeeNombre(nom,15,7,40);
				mvprintw(10,10,"¿Con cuántos discos deseas jugar? 1-8 ");
				leerdiscos(&numdiscos);
				nd=numdiscos;
				todis=nd;
				erase();
				definir_planos();
				draw_planos();
				draw_torres();
				build_torres();
				iniciar_esquinas();
				pintar_torre();
				refresh();
				jugarha(nd,nom,&now,&des);
				DIF = difftime( des,now);
				LeeImpreRancking(lista, nd,opm);
				InsertarDato(lista,nom,DIF,nd);
				clear();
				MandarImpress(lista, nd, opm);
				break;
			case 2:
				clear();
				titulo("Ranking del juego de hanoi",2,3,5);
				mvprintw(8,6,"\tIntroduzca el número de disco del rancking que deesa ver : ");
				echo();
				scanw("%d",&dis);
				LeeImpreRancking(lista,dis, opm);
				break;
			case 3:
				break;
		}
		refresh();
		erase();
		m=0;
	}while(opm!=3);
	endwin();
	return 0;
}


void LeeNombre(char *cad,int max,int r, int c)
/*Descripci¢n: Este procedimiento permite capturar un nombre, validando
 *caracter por caracter, solo se permiten letras del alfabeto en ingls
 *y espacios en blanco.
*/
{
	int aux;
	int ind, band;
	noecho();
	keypad(stdscr, TRUE);
	ind = band =0;
	while (!band && ind < max -1){
		do{
			move(r,c+ind);
			aux = getch();
		}while (aux != RET && aux != ENT && !isalpha(aux));
		switch(aux){
			case ENT:
				if(ind>0){
					band = 1;
					cad[ind] = '\0';
				}
				break;
			case RET:
				if (ind > 0){
					ind--;
					move(r,c+ind);
					printw(" ");
					move(r,c+ind);
				}
				break;
			default :		
				if(ind==0){
					cad[ind] = toupper(aux);
					aux=toupper(aux);
					move(r,c+ind);
					printw("%c",aux);
					ind++;
				}else{
					cad[ind] = aux;
					move(r,c+ind);
					printw("%c",aux);
					ind++;
				}
		}
	}
	if (ind == max -1){
		mvprintw(r+1,c,"Tamaño máximo de la cadena alcanzado");
		cad[ind] = '\0';
	}
}


void LeeEntero(char *cad,int max,int r, int c)
{
	int aux;
	int ind, band;
	noecho();
	keypad(stdscr, TRUE);
	ind = band =0;
	while (!band && ind < max -1){
		do{
			move(r,c+ind);
			aux = getch();
		}while (aux != RET && aux != ENT && !isdigit(aux));
		switch(aux){
			case ENT:
				band = 1;
				cad[ind] = '\0';
				break;
			case RET:
				if (ind > 0){
					ind--;
					move(r,c+ind);
					printw(" ");
					move(r,c+ind);
				}
				break;
			default :
				cad[ind] = aux;
				move(r,c+ind);
				printw("%c",aux);
				ind++;
		}
	}
	if (ind == max -1)
		cad[ind] = '\0';
}


int LeeInt(int r, int c){
	char cad[12];
	int valor;
	mvprintw(r,c,"                         ");
	LeeEntero(cad,12,r,c);
	valor = atoi(cad);
	return valor;
}

void Centrar(char *cad, int r)
{
	int ren,col;
	getmaxyx(stdscr, ren, col);
	ren++;
	col = (col / 2) - (strlen(cad) / 2);
	mvprintw(r,col,"%s",cad);
}

void Derecha(char *cad, int r)
{
	int ren,col;
	getmaxyx(stdscr, ren, col);
	ren++;
	col = col- strlen(cad);
	mvprintw(r,col,"%s",cad);
}

void Error(char *cad, int r)
{
	int ren,col,i;
	getmaxyx(stdscr, ren, col);
	ren++;
	col = (col / 2) - (strlen(cad) / 2);
	attron(A_BLINK);
	mvprintw(r,col,"%s",cad);
	getch();
	attroff(A_BLINK);
	for (i=0;i<strlen(cad);i++)
		mvprintw(r,col+i,"  ");
	
}
int Menu1()
{
	int i;
	Centrar("######################################",4);
	Centrar("by : linuxitos",2);
	Centrar("BIENVENIDO",5);
	mvprintw(7,16,"1.- Jugar");
	mvprintw(8,16,"2.- Ver ranking");
	mvprintw(9,16,"3.- Salir ");
	attroff(COLOR_PAIR(2));
	mvprintw(10,16,"                         ");
	mvprintw(10,16,"Elije una opción");
	i=Rango(1,3,10,33);
	return i;
}
int Rango(int men , int may,int r,int c)
{
	int h=0,valor;
	char cad[4];
	while(h==0){
		echo();
		mvprintw(r,c,"                         ");
		LeeEntero(cad,4,r,c);
		valor = atoi(cad);
		if(valor>=men && valor<=may){
			 h=1;
		}
	}
	return valor;
}

int AvisoConfirmacion(char *cad)
{
	int i=0,b=0;
	char resp;
	while(b==0){
		echo();
		mvprintw(4,6,"%s",cad);
		mvscanw(5,6,"%c",&resp);
		if(resp=='s' || resp=='S'){
			i=1;
			b=1;
		}else{
			b=1;
		}
	}
	clear();
	return  i;
}

void titulo(char *impre,int r1, int r2, int r3)
{
	Centrar(impre,r1);
	Centrar("By : linuxitos",r2);
	Centrar("Informática",r3);
}

void iniciando()
{
	echo();
	refresh();
	printw("\n\tIniciando programas...");
	echo();
	usleep(2);
	printw("\n\tIniciando gráficos...");
	echo();
	sleep(2);
	printw("\n\tIniciando ventanas...");
	sleep(2);
	printw("\n\tCargando archivos...");
	sleep(2);
	printw("\n\t...");
}

void InsertarDato(Rancking *lista, char *cad, int time, int disco)
{
	int i, j = - 1, band;
	for (i = 0, band = 0; i < P && band == 0; i++)
	{
		if (time <= lista[i].tiempo)
		{
			if(time == lista[i].tiempo)
			{
				j=i;
			}
			if(time < lista[i].tiempo && j == - 1)
			{
				RecorrerDato(lista, i);
				lista[i].nombre = (char *) malloc(strlen(cad));
				lista[i].tiempo = time;
				strcpy(lista[i].nombre, cad);
				lista[i].posicion = i + 1;
				band = 1;
			}
		}
	}
	if(j != -1)
	{
		RecorrerDato(lista, j);
		lista[j+1].nombre = (char *) malloc(strlen(cad));
		lista[j+1].tiempo = time;
		strcpy(lista[j+1].nombre, cad);
		lista[i+1].posicion = j+1;
		
	}
}

void RecorrerDato(Rancking *lista, int ind)
{
	int i;
	for(i= P-2; i >= 0 && i >= ind; i--)
	{
		lista[i+1]=lista[i];
		lista[i+1].posicion = i + 2; 
	}
}

void LeeImpreRancking(Rancking *lista,int ndisco, int opc)
{
	char ndisco1[]="./rancking1.exe";
	char ndisco2[]="./rancking2.exe";
	char ndisco3[]="./rancking3.exe";
	char ndisco4[]="./rancking4.exe";
	char ndisco5[]="./rancking5.exe";
	char ndisco6[]="./rancking6.exe";
	char ndisco7[]="./rancking7.exe";
	char ndisco8[]="./rancking8.exe";
	switch (ndisco)
	{
		case 1:
			if(opc == 1)
			{
				LeerRancking(lista, ndisco1);
			}
			else
			{
				LeerRancking(lista, ndisco1);
				clear();
				ImprimirRancking(lista, ndisco, opc, ndisco1, "Pincipiante");
			}
		break;
		
		case 2:
			if(opc == 1)
			{
				LeerRancking(lista, ndisco2);
			}
			else
			{
				LeerRancking(lista, ndisco2);
				clear();
				ImprimirRancking(lista, ndisco, opc, ndisco2, "Pincipiante");
			}
		break;
		
		case 3:
			if(opc == 1)
			{
				LeerRancking(lista, ndisco3);
			}
			else
			{
				LeerRancking(lista, ndisco3);
				clear();
				ImprimirRancking(lista, ndisco, opc, ndisco3, "Pincipiante");
			}
		break;
		
		case 4:
			if(opc == 1)
			{
				LeerRancking(lista, ndisco4);
			}
			else
			{
				LeerRancking(lista, ndisco4);
				clear();
				ImprimirRancking(lista, ndisco, opc, ndisco4, "Intermedio");
			}
		break;
		
		case 5:
			if(opc == 1)
			{
				LeerRancking(lista, ndisco5);
			}
			else
			{
				LeerRancking(lista, ndisco5);
				clear();
				ImprimirRancking(lista, ndisco, opc, ndisco5, "Intermedio");
			}
		break;
		
		case 6:
			if(opc == 1)
			{
				LeerRancking(lista, ndisco6);
			}
			else
			{
				LeerRancking(lista, ndisco6);
				clear();
				ImprimirRancking(lista, ndisco, opc, ndisco6, "Intermedio");
			}
		break;
		
		case 7:
			if(opc == 1)
			{
				LeerRancking(lista, ndisco7);
			}
			else
			{
				LeerRancking(lista, ndisco7);
				clear();
				ImprimirRancking(lista, ndisco, opc, ndisco7, "Avanzado");
			}
		break;
		
		case 8:
			if(opc == 1)
			{
				LeerRancking(lista, ndisco8);
			}
			else
			{
				LeerRancking(lista, ndisco8);
				clear();
				ImprimirRancking(lista, ndisco, opc, ndisco8, "Avanzado");
			}
		break;
	}
}	

void LeerRancking (Rancking *lista, char *disco)
{
	FILE *arch;
	int total = 0;
	char cad[449];
	if ((arch = fopen(disco, "r"))	 == NULL){
		fprintf(stderr,"\t\tNo se puede abrir el archivo de salida.\n");
	}else{
		while (!feof(arch)){
			if (fgets(cad,449,arch) != NULL){
				LlenandoEstruc(lista, cad, &total);
			}
		}
	}
	fclose(arch);
}

void LlenandoEstruc(Rancking *lista, char *cad, int *total)
{
	int campo, i, j;
	char subc[30];
	for (campo = 0, i = 0, j = 0; i < strlen(cad); i++){
		if (cad[i] != '\t' && cad[i] != '\n'){
			subc[j++]= cad[i];
		}else{
			subc[j] = '\0';
			j = 0;
			campo++;
			switch (campo){
				case 1 : lista[*total].posicion = atoi(subc);
						break;
				case 2 : lista[*total].nombre = (char *) malloc(strlen(subc));
						strcpy(lista[*total].nombre,subc);
						break;
				case 3 : lista[*total].tiempo = atoi(subc);
						break;
			}
		}
	}
	(*total)++;
}

void ImprimirRancking (Rancking *lista, int ndisco, int opc, char *disco, char *nivel)
{
	int col = 9,i,h,m,s,t;
	clear();
	mvprintw(3,28,"R A N C K I N K");
	mvprintw(5, 10,"Numero de Discos : %d ", ndisco);
	mvprintw(5, 38," NIVEL : %s ", nivel);
	mvprintw(7, 16,"Pos");
	mvprintw(7, 25,"Nombre");
	mvprintw(7, 45,"Tiempo H:M:S");
	for(i = 0; i < P; i++)
	{
		mvprintw(col + i, 16,"%d",lista[i].posicion);
		mvprintw(col + i, 25,"%s", lista[i].nombre);
		t = lista[i].tiempo;
		h = t / 3600;
		t = t % 3600;
		m = t / 60;
		t = t % 60;
		s = t % 60;
		mvprintw(col + i, 45,"%d:%d:%d",h,m,s);
	}
	getch();
	if (opc == 1)
	{
		GuardarRancking(lista, disco);
	}
}

void GuardarRancking(Rancking *lista, char *disco)
{
	clear();
	int i;
	FILE *arch;
	printf("\nAbriendo el archivo...\n");
	if ((arch = fopen(disco, "w"))	 == NULL){
		fprintf(stderr, "No se puede abrir el archivo de salida.\n");
		return;
	}
	printf("\nGuardando Profesores del catalogo\n");
	for (i=0; i < P; i++){
		fprintf(arch,"%d\t%s\t%d\n",lista[i].posicion,lista[i].nombre,lista[i].tiempo);
	}
	fclose(arch);
}

void MandarImpress(Rancking *lista, int nd, int opm)
{
	switch(nd){
		case 1:
			ImprimirRancking(lista, nd, opm, "./rancking1.exe", "Pincipiante");
		break;
		
		case 2:
			ImprimirRancking(lista, nd, opm, "./rancking2.exe", "Pincipiante");
		break;
		
		case 3:
			ImprimirRancking(lista, nd, opm, "./rancking3.exe", "Pincipiante");
		break;
		
		case 4:
			ImprimirRancking(lista, nd, opm, "./rancking4.exe", "Intermedio");
		break;
		
		case 5:
			ImprimirRancking(lista, nd, opm, "./rancking5.exe", "Intermedio");
		break;
		
		case 6:
			ImprimirRancking(lista, nd, opm, "./rancking6.exe", "Intermedio");
		break;
		
		case 7:
			ImprimirRancking(lista, nd, opm, "./rancking7.exe", "Avanzado");
		break;
		
		case 8:
			ImprimirRancking(lista, nd, opm, "./rancking8.exe", "Avanzado");
		break;
	}
}

void PilaVacia (Pila *a){
	a->tope=-1;
}
     
int EsVacia (Pila *a){
  if (a->tope==-1)
     return 1;
  else
     return 0;  
}
     
int Cima (Pila a){
	if (!EsVacia(&a))
		return a.arreglo[a.tope];
	else{
		printf("PILA VACIA\n");
		return 0;
	}
}


void Push (int x, Pila *a){
	if (a->tope==todis-1){
		printf("PILA LLENA\n");
	}else {         
		a->tope++;
		a->arreglo[a->tope]=x;
	}   
}
     
int Pop (Pila *a){
	int aux;
	if (EsVacia(a)){
		return 0;
		printf ("PILA VACIA\n");
	}else{
		aux=a->arreglo[a->tope];
		a->tope--;
		return aux;
	}    
}


void jugarha(int numdis,char nom[15],time_t *now,time_t *des)
{
	char a='a', b='b',c='c';
	Pila p1,p2,p3;
	int i,mv=2,band=0,tin=0,nunv=0,aux,aux2,aux3,tor,tor1;
	PilaVacia(&p1);
	PilaVacia(&p2);
	PilaVacia(&p3);
	for (i=numdis-1; i>=0;i--){
        Push(i,&p1);
    }
    for(i=1;i<numdis;i++){
		mv=mv*2;
	}
	mv--;
	while(band==0){
		echo();
		refresh();
		Centrar("JUEGO LAS TORRES DE HANOI",1);
		mvprintw(2,30,"Player: %s ",nom);
		mvprintw(filas+3,24,"Mínimo de movimientos %d ",mv);
       	mvprintw(filas+4,24,"Movimientos realizados %d ",nunv);
		echo();
		imtorres(numdis);
		leertorre(&tor,6,"Mover disco de torre ");
		if(tin==0){
			*now=time(0);
			tin=1;
		}
		leertorre(&tor1,7,"A   la   torre ");
		if(tor==tor1){
			Error("Moviemiento no válido",filas+10);
			mvprintw(filas+4,24,"Movimientos realizados %d ",nunv);
		}else{
			switch(tor){
				case 1:
					if(tor1==2){
						if(!EsVacia(&p1)){
							if(EsVacia(&p2)){
								aux3=Pop(&p1);
								Push(aux3,&p2);
								show_hanoi(a%97,b%97);
							}else{
								if(!EsVacia(&p2)){
									aux=Cima(p1);
									aux2=Cima(p2);
									if(aux<aux2){
										aux3=Pop(&p1);
										Push(aux3,&p2);
										show_hanoi(a%97,b%97);
									}else{
										echo();
										Error("Moviemiento no válido",filas+15);
									}
								}
							}
						}else{
							echo();
							Error("Moviemiento no válido",filas+15);
						}
					}else{
						if(!EsVacia(&p1)){
							if(EsVacia(&p3)){
								aux3=Pop(&p1);
								Push(aux3,&p3);
								show_hanoi(a%97,c%97);
							}else{
								if(!EsVacia(&p3)){
									aux=Cima(p1);
									aux2=Cima(p3);
									if(aux<aux2){
										aux3=Pop(&p1);
										Push(aux3,&p3);
										show_hanoi(a%97,c%97);
									}else{
										echo();
										Error("Moviemiento no válido",filas+15);
									}
								}
							}
						}else{
							echo();
							Error("Moviemiento no válido",filas+15);
						}
					}
					break;
				case 2:
					if(tor1==1){
						if(!EsVacia(&p2)){
							if(EsVacia(&p1)){
								aux3=Pop(&p2);
								Push(aux3,&p1);
								show_hanoi(b%97,a%97);
							}else{
								if(!EsVacia(&p1)){
									aux=Cima(p2);
									aux2=Cima(p1);
									if(aux<aux2){
										aux3=Pop(&p2);
										Push(aux3,&p1);
										show_hanoi(b%97,a%97);
									}else{
										Error("Moviemiento no válido",filas+15);
									}
								}
							}
						}else{
							Error("Moviemiento no válido",filas+15);
						}
					}else{
						if(!EsVacia(&p2)){
							if(EsVacia(&p3)){
								aux3=Pop(&p2);
								Push(aux3,&p3);
								show_hanoi(b%97,c%97);
							}else{
								if(!EsVacia(&p3)){
									aux=Cima(p2);
									aux2=Cima(p3);
									if(aux<aux2){
										aux3=Pop(&p2);
										Push(aux3,&p3);
										show_hanoi(b%97,c%97);
									}else{
										echo();
										Error("Moviemiento no válido",filas+15);
									}
								}
							}
						}else{
							echo();
							Error("Moviemiento no válido",filas+15);
						}
					}
					break;
				case 3:
					if(tor1==1){
						if(!EsVacia(&p3)){
							if(EsVacia(&p1)){
								aux3=Pop(&p3);
								Push(aux3,&p1);
								show_hanoi(c%97,a%97);
							}else{
								if(!EsVacia(&p1)){
									aux=Cima(p3);
									aux2=Cima(p1);
									if(aux<aux2){
										aux3=Pop(&p3);
										Push(aux3,&p1);
										show_hanoi(c%97,a%97);
									}else{
										Error("Moviemiento no válido",filas+15);
									}
								}
							}
						}else{
							echo();
							Error("Moviemiento no válido",filas+15);
						}
					}else{
						if(!EsVacia(&p3)){
							if(EsVacia(&p2)){
								aux3=Pop(&p3);
								Push(aux3,&p2);
								show_hanoi(c%97,b%97);
							}else{
								if(!EsVacia(&p2)){
									aux=Cima(p3);
									aux2=Cima(p2);
									if(aux<aux2){
										aux3=Pop(&p3);
										Push(aux3,&p2);
										show_hanoi(c%97,b%97);
									}else{
										Error("Moviemiento no válido",filas+15);
									}
								}
							}
						}else{
							Error("Moviemiento no válido",filas+15);
						}
					}
					break;
			}//llave de casos
		}
		nunv++;
		mvprintw(filas+4,24,"Movimientos realizados %d ",nunv);
		if(EsVacia(&p1)==1){
			if(EsVacia(&p2)==1){
				band=1;
				*des=time(0);
				echo();
				Centrar("Ganaste",14);
				Centrar(nom,16);
			}
		}
		if(EsVacia(&p1)==1){
			if(EsVacia(&p3)==1){
				band=1;
				*des=time(0);
				echo();
				Centrar("Ganaste",14);
				Centrar(nom,16);
			}
		}
	}
	Error("Presiona una tecla para salir",filas+11);
}

void imtorres(int numdis){
	if(numdis==1){
			mvprintw(filas+1,2,"Torre 1   Torre 2  Torre  3");
	}
	if(numdis==2){
		mvprintw(filas+1,2,"Torre 1    Torre 2   Torre  3");
	}
	if(numdis==3){
		mvprintw(filas+1,4,"Torre 1       Torre 2      Torre  3");
	}
	if(numdis==4){
		mvprintw(filas+1,5,"Torre 1        Torre 2       Torre  3");
	}
	if(numdis==5){
		mvprintw(filas+1,6,"Torre 1          Torre 2         Torre  3");
	}
	if(numdis==6){
		mvprintw(filas+1,7,"Torre 1          Torre 2         Torre  3");
	}
	if(numdis==7){
		mvprintw(filas+1,8,"Torre 1            Torre 2             Torre  3");
	}
	if(numdis==8){
		mvprintw(filas+1,9,"Torre 1               Torre 2              Torre  3");
	}
}

void leerdiscos(int *numdiscos){
	int band=0;
	do{
		*numdiscos=LeeInt(10,50);
		if(*numdiscos>=1 && *numdiscos<=8){
			band=1;
		}else{
			Error("Número de discos no válido",filas+15);	
			mvprintw(10,48,"                         ");
		}
	}while(band!=1);
}
void leertorre(int *tor,int i,char *me)
{
	int band=0;
	do{
		refresh();
		mvprintw(filas+i,24," %s ",me);
		mvprintw(filas+i,48,"        ");
		*tor=LeeInt(filas+i,48);
		if(*tor>=1 && *tor<=3){
			band=1;
		}else{
			Error("Número de torre no válido",filas+15);	
		}
	}while(band!=1);
}

/*Bloque de módulos genarar una torre indice de donde se puede seleccionar un nivel e imprimirlo.*/
void definir_planos(void){
	int i;
	//planostorres es una matriz y contiene el numero de discos mas 2
	planostorres=(int**)calloc(numdiscos+2, sizeof(int*));

	for(i=0;i<=numdiscos+1;i++)
		*(planostorres+i)=(int*)calloc(2, sizeof(int));
}

void draw_planos(){
	int i,j, caracteres=2, espacios=numdiscos+1;
	for(i=0;i<=numdiscos+1;i++){
		for(j=0;j<2;j++){
			if(j==0){
				planostorres[i][j]=espacios;		 
				espacios--;
			}else{  				
				planostorres[i][j]=caracteres;
				caracteres=caracteres+2;
			}
		}
	}
}

void draw_torres(void)
{
	int i,j,numcaracteres,centrov1,centrov2;
	torre=(char**)calloc(numdiscos+2, sizeof(char*));
	for(i=0;i<=numdiscos;i++){
		numcaracteres=i*2+2;
		*(torre+i)=(char*)calloc(numcaracteres, sizeof(char));
	}

	*(torre+i)=(char*)calloc(numcaracteres+4, sizeof(char));
	
	for(i=0;i<(numdiscos+1)*2+2;i++){
		torre[numdiscos+1][i]=' ';
	}
	torre[numdiscos+1][i]='\0';
	for(i=0;i<=numdiscos;i++){
	 	 //Añadir Espacios
		numcaracteres=i*2+2;
		centrov1=i;
	  	centrov2=i+1;
	  	for(j=0;j<numcaracteres;j++){
  			if((j==centrov1 || j==centrov2)&&i==0){
 				torre[i][j]=' ';
 			}else{
				torre[i][j]=' ';
			}
	  	}
	  	torre[i][j]='\0';
	}	 
}

void iniciar_esquinas(void)
{
	esquinaA=2;
	esquinaB=esquinaA+planostorres[numdiscos][1]+D;
	esquinaC=esquinaB+planostorres[numdiscos][1]+D;
}

void pintar_torre(void)
{
	int i;
	filas=4;
	//Imprimiendo niveles superiores .
	for(i=0;i<=numdiscos;i++){
		imprimir_nivel(torres[i][0],filas,esquinaA+planostorres[torres[i][0]][0]);	//TorreA	
		imprimir_nivel(torres[i][1],filas,esquinaB+planostorres[torres[i][1]][0]);	//TorreB
		imprimir_nivel(torres[i][2],filas,esquinaC+planostorres[torres[i][2]][0]);	//TorreC
		filas++;
	}
	//Imprimiendo nivel base.
	imprimir_nivel(numdiscos+1,filas, esquinaA);
	imprimir_nivel(numdiscos+1,filas, esquinaB);
	imprimir_nivel(numdiscos+1,filas, esquinaC);
}

void show_hanoi(int origen, int destino){
	if(origen==0){
		switch(destino){
			case 1:
				mover_disco(origen, destino,&topeA,&topeB);
				break;
			case 2:
				mover_disco(origen, destino,&topeA,&topeC);
				break;
		}
	}
	if(origen==1){
		switch(destino){
			case 0:
				mover_disco(origen, destino,&topeB,&topeA);
				break;
			case 2:
				mover_disco(origen, destino,&topeB,&topeC);
				break;
		}
	}
		  
	if(origen==2){
		switch(destino){
			case 0:
				mover_disco(origen, destino,&topeC,&topeA);
				break;
			case 1:
				mover_disco(origen, destino,&topeC,&topeB);
				break;
		}
	}
}

void paleta_colores(void){
	if (has_colors()){
		init_pair(1,COLOR_RED,COLOR_GREEN);
		init_pair(2,COLOR_RED,COLOR_MAGENTA);
		init_pair(3,COLOR_RED,COLOR_CYAN);
		init_pair(4,COLOR_BLACK,COLOR_RED);
		init_pair(5,COLOR_BLACK,COLOR_BLUE);
		init_pair(6,COLOR_RED,COLOR_YELLOW);
		init_pair(7,COLOR_WHITE,COLOR_BLACK);
		init_pair(8,COLOR_BLUE,COLOR_WHITE);
	}
}

void mover_disco(int x_origen, int x_destino, int* topeOrigen, int* topeDestino){
	int auxiliar; 
	/*Aquí se quita el disco de la torre de origen*/
	m++;
	pintar_torre();
	refresh();
	erase();
	auxiliar=torres[*topeOrigen][x_origen];
	torres[*topeOrigen][x_origen]=0;	
	pintar_torre();
	refresh();
	erase();
	*topeOrigen=*topeOrigen+1;
	/*Aquí añade el disco a la torre de destino*/
	*topeDestino=*topeDestino-1;
	torres[*topeDestino][x_destino%3]=auxiliar;
	pintar_torre();
	refresh();
}

void	imprimir_nivel(int nivel, int fila,int columnas){
	int color=nivel%6+1;
	if(nivel==0 || nivel==numdiscos+1){
		color=7;
	}
	attron(COLOR_PAIR(color));
	mvprintw(fila, columnas,"%s", torre[nivel]);
	attroff(COLOR_PAIR(color));
}

void imprimirprese(void ){
	Centrar("BIENVENIDOS AL JUEGO DE LAS TORRES DE HANOI",1);
	Centrar("INSTRUCCIONES",3);
	Centrar("Debes pasar todos los discos de la torre 1",5);
	Centrar("a cualquiera de  las torres (torre2, torre3)",7);
	Centrar("ojo, deben de estar en el mismo orden que al principio",8);
	Centrar("evitando poner un disco de mayor tamaño sobre uno de menor tamaño.",9);
	Centrar("Puedes auxiliarte de las torres 2 y 3 ",11);
	Centrar("para hacer los movimientos que consideres necesarios.",13);
	Centrar("*****************************************************************************",17);		
	Centrar("Presione una tecla para continuar",20);
	getch();
}

void draw_menubar(WINDOW *menubar){
    wbkgd(menubar,COLOR_PAIR(4));
    waddstr(menubar,"                      Licenciatura en Informática");
    wattron(menubar,COLOR_PAIR(5));
    wattroff(menubar,COLOR_PAIR(5));
    wmove(menubar,0,0);
}

void	build_torres(void){
	int i,j;
	torres=(int**)calloc(numdiscos+1, sizeof(int*));

	for(i=0;i<=numdiscos;i++){
		*(torres+i)=(int*)calloc(3, sizeof(int));
	}

	for(i=0;i<=numdiscos;i++){
		for(j=0;j<3;j++){
			if(j==0)
				torres[i][j]=i;
			else
				torres[i][j]=0;
		}
	}
	/*Inicializando Topes*/
	topeA=1;
	topeB=numdiscos+1;
	topeC=numdiscos+1;
}
